#include <string.h>
#include <stdio.h>
#include <stdlib.h>


#define TAM 100


typedef struct nodo_colaTDA{
	void *elemento;
	struct nodo_colaTDA *siguiente;
	struct nodo_colaTDA *anterior;
} nodo_cola;

typedef struct colaTDA{
	unsigned long tamano;
	struct nodo_colaTDA *inicio;
	struct nodo_colaTDA *fin;
} cola;



/*nodo_cola* CrearNodo(void*elemento){
        nodo_cola* nodo=(nodo_cola*)malloc(sizeof(nodo_cola));
        nodo -> elemento=elemento;
        nodo -> siguiente=NULL;
        nodo -> anterior=NULL;
        return nodo;
}

void destruirNodo(nodo_cola* nodo){
        nodo -> elemento=NULL;
        nodo -> siguiente=NULL;
        free(nodo);
}*/


cola *crear_cola(){
        cola *pcola= malloc(sizeof(cola));
	if(pcola==NULL){
		return NULL;
	}else{
		(*pcola).inicio=NULL;
		(*pcola).fin=NULL;
		(*pcola).tamano=0;
        return pcola;
	}
}


unsigned long tamano_cola(cola *puncola){
	return (*puncola).tamano;
}

int encolar(cola *puncola, void *elemento){
	if(elemento!=NULL){
		nodo_cola *nodo;
		nodo=malloc(sizeof(nodo_cola));
		(*nodo).elemento=elemento;
		if((*puncola).tamano==0){
			nodo->anterior=nodo;
			nodo->siguiente=nodo;
			(*puncola).tamano++;
			(*puncola).inicio=nodo;
			(*puncola).fin=nodo;
			return 0;
		}else{
			(*nodo).anterior=(*puncola).fin;
			(*(*puncola).fin).siguiente=nodo;
			(*puncola).fin=nodo;
			(*puncola).tamano++;
			return 0;
		}
	}else{
		return -1;
	}
}

void *decolar(cola *puncola){
	if((*puncola).tamano!=0){
		nodo_cola *nodo=(*puncola).inicio;
		void *nod=(*nodo).elemento;
		(*puncola).inicio=(*nodo).siguiente;
		free(nodo);
		(*puncola).tamano--;
		return 	nod;
	}else{
		return NULL;
	}

}


/*void eliminar(cola* cola){
        if(cola -> inicio){
                nodo_cola* eliminado = cola -> inicio;
                cola -> inicio=cola -> inicio -> siguiente;
                destruirNodo(eliminado);
                if(!cola -> inicio)
                        cola -> fin=NULL;
        }
}

void destruir_cola_opcional(cola* cola){
        while(cola -> inicio){
                eliminar(cola);
        }
        free(cola);
}*/

unsigned long posicion_cola(cola *puncola, void *element){
	if (element == NULL || puncola -> inicio == NULL){
		return -1;
	}else if (puncola -> inicio -> elemento == element){
		return 0;
	}else if (puncola -> fin -> elemento == element){
		return puncola -> tamano -1;
	}

	nodo_cola *pmovible = puncola -> inicio;
	unsigned long position = 0;
	while (pmovible -> siguiente){
		if (pmovible -> elemento == element){
			return position;
		}
		pmovible = pmovible -> siguiente;
		position = position + 1;
	}
	return -1;
}

int destruir_cola(cola *puncola){
	if(puncola -> tamano == 0){
		free(puncola);
		return 0;
	}else{
		return -1;
	}
}






int main(){
        char entrada[TAM];
        int *numero;
        int busqueda;
        cola *pun;
	pun = crear_cola();

	printf("Ingrese los valores que ingresara en la cola, cuando quiera dejar de ingresar presione la letra x: \n");

        while(1){
                memset(entrada, 0, TAM);
                fgets(entrada,TAM,stdin);
                if(strcmp(entrada,"x\n")==0){
                        break;
                }
                else{
                        numero = malloc(sizeof(int));
                        encolar(pun, numero);
                        *numero = atoi(entrada);
                }
        }
        printf("La cola creada contiene %lu elementos\n", tamano_cola(pun));
        printf("Indique el elemento que quiere encontrar: ");
        scanf("%d", &busqueda);

        nodo_cola *nodo;
        nodo = pun -> inicio;
        while(nodo != NULL){
                if(*((int *)nodo -> elemento) == busqueda){
                        printf("El elemento indicado se encuentra en la posicion %lu \n", posicion_cola(pun, nodo -> elemento));
                        break;
                }
        nodo= nodo -> siguiente;
        }
        do{
                numero = decolar(pun);
                free(numero);
        }while(tamano_cola(pun) > 0);
                printf("Se decolaron los siguientes elementos: \n");
        	destruir_cola(pun);
}
